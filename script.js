
function getOffset(el) {
  el = el.getBoundingClientRect();
  return {
    left: el.left + window.scrollX,
    top: el.top + window.scrollY
  }
}

// Wait until the DOM is loaded
document.addEventListener("DOMContentLoaded", function(e) {

    var menuItems = document.getElementsByClassName("js-menuItem");
    var scrollItems = [];

    for (i=0; i < menuItems.length; i++) {

        //console.log(menuItems[i].dataset.section);
        var el = document.getElementById(menuItems[i].dataset.section);
        scrollItems.push({
            menuItem: menuItems[i],
            sectionItem: el
        })
    }

    var currentSection = scrollItems[0];
    var newSection = scrollItems[0];

    window.addEventListener('scroll', function(e) {

        //console.log("scrollY " + window.scrollY);
        //console.log("Scrolling: " + getOffset(about).top );
        // console.log(contact.dataset.menuItem);


        for (i=0; i < scrollItems.length; i++) {

            if (window.scrollY > getOffset(scrollItems[i].sectionItem).top * 0.7) {

                //console.log(scrollItems[i].sectionItem.id + " hej");
                newSection = scrollItems[i];
            }
        }

        if (currentSection != newSection) {

            currentSection.menuItem.classList.toggle("nav-focus");
            newSection.menuItem.classList.toggle("nav-focus");
            currentSection = newSection;
            console.log("change section");
        }

    })


})
